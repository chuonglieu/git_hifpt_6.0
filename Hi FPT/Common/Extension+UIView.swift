//
//  Extension+UIView.swift
//  Hi FPT
//
//  Created by Developer on 05/07/2021.
//

import UIKit

extension UIView {
    
    func roundCorner(borderColor: UIColor, borderWidth: Float, cornerRadius: CGFloat) {
        self.layer.cornerRadius  = cornerRadius
        self.layer.masksToBounds = true
        self.layer.borderWidth   = CGFloat(borderWidth)
        self.layer.borderColor   = borderColor.cgColor
    }
    
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 5
      }
    
    func addColors(colors: [UIColor], withPercentage percentages: [Double]) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        var colorsArray: [CGColor] = []
        var locationsArray: [NSNumber] = []
        var total = 0.0
        locationsArray.append(0.0)
        for (index, color) in colors.enumerated() {
            // append same color twice
            colorsArray.append(color.cgColor)
            colorsArray.append(color.cgColor)
            // Calculating locations w.r.t Percentage of each
            if index+1 < percentages.count{
                total += percentages[index]
                let location: NSNumber = NSNumber(value: total/100)
                locationsArray.append(location)
                locationsArray.append(location)
            }
        }
        locationsArray.append(1.0)
        gradientLayer.colors = colorsArray
        gradientLayer.locations = locationsArray
        self.backgroundColor = .clear
        self.layer.addSublayer(gradientLayer)
    }
    
    func addDashedBorder() {
            //Create a CAShapeLayer
            let shapeLayer = CAShapeLayer()
            shapeLayer.strokeColor = UIColor.lightGray.cgColor
            shapeLayer.lineWidth = 1
         
            shapeLayer.lineDashPattern = [2,3]

            let path = CGMutablePath()
            path.addLines(between: [CGPoint(x: 0, y: 0),
                                    CGPoint(x: 0, y: self.frame.height)])
            shapeLayer.path = path
            layer.addSublayer(shapeLayer)
        }
    private static let lineDashPattern: [NSNumber] = [4, 4]
    private static let lineDashWidth: CGFloat = 0.5

       func makeDashedBorderLine() {
           let path = CGMutablePath()
           let shapeLayer = CAShapeLayer()
           shapeLayer.lineWidth = UIView.lineDashWidth
           shapeLayer.strokeColor = UIColor.lightGray.cgColor
           shapeLayer.lineDashPattern = UIView.lineDashPattern
           path.addLines(between: [CGPoint(x: 0, y: 0),
                                   CGPoint(x: 1000, y: 0)])
           shapeLayer.path = path
           layer.addSublayer(shapeLayer)
       }
}

