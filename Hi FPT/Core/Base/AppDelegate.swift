//
//  AppDelegate.swift
//  Hi FPT
//
//  Created by TaiVo on 30/06/2021.
//

import UIKit
import IQKeyboardManagerSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func infoForKey(_ key: String) -> String? {
            return (Bundle.main.infoDictionary?[key] as? String)?
                .replacingOccurrences(of: "\\", with: "")
     }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        let serverURL = infoForKey("ServerUrl")
        print(serverURL!)
        let mWindow = UIWindow()
        self.window = mWindow
        self.window?.rootViewController = MyTabbarController()
        self.window?.makeKeyAndVisible()
        return true
    }

}

