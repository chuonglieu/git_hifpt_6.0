//
//  TabNavigationMenu.swift
//  Hi FPT
//
//  Created by TaiVo on 30/06/2021.
//

import UIKit
import Lottie

class TabNavigationMenu: UIView {

    var itemTapped: ((_ tab: Int) -> Void)?
    var activeItem: Int = 0
    var items:[TabItem] = []
    var imgViewTabbar:UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    convenience init(menuItems: [TabItem], frame: CGRect) {
        self.init(frame: frame)
        items = menuItems
        imgViewTabbar = UIImageView(frame: CGRect(x: 0, y: 30, width: frame.width, height: frame.height - 30))
        imgViewTabbar.image = UIImage(named: "bg_tabbar")
        imgViewTabbar.isUserInteractionEnabled = true
        self.addSubview(imgViewTabbar)

        for i in 0 ..< items.count {
            let itemWidth = self.frame.width / CGFloat(menuItems.count)
            let leadingAnchor = itemWidth * CGFloat(i)

            let itemView = self.createTabItem(item: menuItems[i])
            itemView.tag = i
            
            if items[i] == .scanQR{
                self.addSubview(itemView)
                NSLayoutConstraint.activate([
                    itemView.heightAnchor.constraint(equalTo: self.heightAnchor),
                    itemView.widthAnchor.constraint(equalToConstant: itemWidth), // fixing width

                    itemView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: leadingAnchor),
                    itemView.topAnchor.constraint(equalTo: self.topAnchor),
                ])
                print(itemView.tag)
            }else{
                imgViewTabbar.addSubview(itemView)
                NSLayoutConstraint.activate([
                    itemView.heightAnchor.constraint(equalTo: imgViewTabbar.heightAnchor),
                    itemView.widthAnchor.constraint(equalToConstant: itemWidth), // fixing width

                    itemView.leadingAnchor.constraint(equalTo: imgViewTabbar.leadingAnchor, constant: leadingAnchor),
                    itemView.topAnchor.constraint(equalTo: imgViewTabbar.topAnchor),
                ])
            }
            
            itemView.translatesAutoresizingMaskIntoConstraints = false
            itemView.clipsToBounds = true
        }
        
        
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
        self.activateTab(tab: 0)
    }
    
    func createTabItem(item: TabItem) -> UIView {
        if item == .scanQR{
            let tabBarItem = UIView(frame: CGRect.zero)
            let animationView = AnimationView()
            
            let animation = Animation.named("scanQR_animation")
            animationView.animation = animation
            animationView.contentMode = .scaleAspectFit
            animationView.backgroundBehavior = .pauseAndRestore
            animationView.translatesAutoresizingMaskIntoConstraints = false
            tabBarItem.addSubview(animationView)
//
//            tabBarItem.translatesAutoresizingMaskIntoConstraints = false
//            tabBarItem.clipsToBounds = true
            
            NSLayoutConstraint.activate([
                animationView.topAnchor.constraint(equalTo: tabBarItem.topAnchor, constant: 0),
//                itemIconView.topAnchor.constraint(equalTo: tabBarItem.lastBaselineAnchor, constant: 12),
                animationView.widthAnchor.constraint(equalToConstant: 64),
                animationView.heightAnchor.constraint(equalToConstant: 64),
                animationView.centerXAnchor.constraint(equalTo: tabBarItem.centerXAnchor),
            ])
            
            animationView.play(fromProgress: 0,
                               toProgress: 1,
                               loopMode: LottieLoopMode.loop,
                               completion: { (finished) in
                                if finished {
                                  print("Animation Complete")
                                } else {
                                  print("Animation cancelled")
                                }
            })
            animationView.tag = 2
            animationView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleTap))) // Each item should be able to trigger and action on tap
            return tabBarItem
            
        }else{
            let tabBarItem = UIView(frame: CGRect.zero)
            let itemTitleLabel = UILabel(frame: CGRect.zero)
            let itemIconView = UIImageView(frame: CGRect.zero)
            
            tabBarItem.tag = 11
            itemTitleLabel.tag = 12
            itemIconView.tag = 13
            
            itemTitleLabel.text = item.displayTitle
            itemTitleLabel.textColor = UIColor(red: 0.392, green: 0.392, blue: 0.396, alpha: 1)
            itemTitleLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
            itemTitleLabel.textAlignment = .center
            itemTitleLabel.translatesAutoresizingMaskIntoConstraints = false
            itemTitleLabel.clipsToBounds = true
                
            itemIconView.image = item.icon.withRenderingMode(.automatic)
            itemIconView.translatesAutoresizingMaskIntoConstraints = false
            itemIconView.clipsToBounds = true
            itemIconView.contentMode = .scaleToFill
    //        tabBarItem.layer.backgroundColor = UIColor.white.cgColor
            tabBarItem.addSubview(itemIconView)
            tabBarItem.addSubview(itemTitleLabel)
            tabBarItem.translatesAutoresizingMaskIntoConstraints = false
            tabBarItem.clipsToBounds = true
            NSLayoutConstraint.activate([
                itemIconView.topAnchor.constraint(equalTo: tabBarItem.topAnchor, constant: 12),
                itemIconView.heightAnchor.constraint(equalToConstant: 24), // Fixed height for our tab item(25pts)
                itemIconView.widthAnchor.constraint(equalToConstant: 24), // Fixed width for our tab item icon
                itemIconView.centerXAnchor.constraint(equalTo: tabBarItem.centerXAnchor),
                itemTitleLabel.heightAnchor.constraint(equalToConstant: 13), // Fixed height for title label
                itemTitleLabel.widthAnchor.constraint(equalTo: tabBarItem.widthAnchor), // Position label full width across tab bar item
                itemTitleLabel.topAnchor.constraint(equalTo: itemIconView.bottomAnchor, constant: 6), // Position title label 4pts below item icon
            ])
            tabBarItem.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleTap))) // Each item should be able to trigger and action on tap
            return tabBarItem
        }
        
    }
    
    @objc func handleTap(_ sender: UIGestureRecognizer) {
        self.switchTab(from: self.activeItem, to: sender.view!.tag)
    }
    
    func switchTab(from: Int, to: Int) {
        self.deactivateTab(tab: from)
        self.activateTab(tab: to)
    }
    
    func activateTab(tab: Int) {
        if items[tab] != .scanQR{
            var index = tab
            switch items[tab] {
            case .news:
                index = 2
                break
            case .personal:
                index = 3
                break
            default:
                index = tab
                break
            }
            
            let tabToActivate = imgViewTabbar.subviews[index]
            //UILabel
            (tabToActivate.viewWithTag(12) as? UILabel)?.textColor = UIColor(red: 0.27, green: 0.394, blue: 0.929, alpha: 1)
            
            //UIImageView
            (tabToActivate.viewWithTag(13) as? UIImageView)?.image = items[tab] .iconSelected.withRenderingMode(.automatic)
            
            let borderWidth = tabToActivate.frame.size.width - 40
            let borderLayer = CALayer()
            borderLayer.backgroundColor = UIColor(red: 0.27, green: 0.394, blue: 0.929, alpha: 1).cgColor
            borderLayer.name = "active border"
            borderLayer.frame = CGRect(x: 20, y: 4, width: borderWidth, height: 2)
            DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.8, delay: 0.0, options: [.curveEaseIn, .allowUserInteraction], animations: {
                        tabToActivate.layer.addSublayer(borderLayer)
                        tabToActivate.setNeedsLayout()
                        tabToActivate.layoutIfNeeded()
                    })
                    self.itemTapped?(tab)
                }
                self.activeItem = tab
        }else{
            self.itemTapped?(tab)
        }
       
    }
    
    func deactivateTab(tab: Int) {
        if items[tab] != .scanQR{
            var index = tab
            switch items[tab] {
            case .news:
                index = 2
                break
            case .personal:
                index = 3
                break
            default:
                index = tab
                break
            }
            
            let inactiveTab = imgViewTabbar.subviews[index]
            //UILabel
            (inactiveTab.viewWithTag(12) as? UILabel)?.textColor = UIColor(red: 0.392, green: 0.392, blue: 0.396, alpha: 1)
            
            //UIImageView
            (inactiveTab.viewWithTag(13) as? UIImageView)?.image = items[tab] .icon.withRenderingMode(.automatic)
            
            let layersToRemove = inactiveTab.layer.sublayers!.filter({ $0.name == "active border" })
            DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.4, delay: 0.0, options: [.curveEaseIn, .allowUserInteraction], animations: {
                        layersToRemove.forEach({ $0.removeFromSuperlayer() })
                        inactiveTab.setNeedsLayout()
                        inactiveTab.layoutIfNeeded()
                    })
            }
        }
    }
}
