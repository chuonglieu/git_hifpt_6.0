//
//  TabItem.swift
//  Hi FPT
//
//  Created by TaiVo on 30/06/2021.
//

import UIKit

enum TabItem: String, CaseIterable {
    case home = "Trang chủ"
    case messages = "Tin nhắn"
    case scanQR = "Scan"
    case news = "Tin tức"
    case personal = "Cá nhân"
    
    var viewController: UIViewController {
            switch self {
            case .home:
                return UIStoryboard.init(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeController") as! HomeController
        
            case .messages:
                return MessagesController()
            case .scanQR:
                return UIViewController()
            case .news:
                return NewsController()
            case .personal:
                return PersonalController()
            }
        }
    
    // these can be your icons
    var icon: UIImage {
        switch self {
        case .home:
            return UIImage(named: "item_home")!
    
        case .messages:
            return UIImage(named: "item_messages")!
        case .scanQR:
            return UIImage(named: "item_messages")!
        case .news:
            return UIImage(named: "item_news")!
        case .personal:
            return UIImage(named: "item_personal")!
        }
    }
    
    // these can be your icons selected
    var iconSelected: UIImage {
        switch self {
        case .home:
            return UIImage(named: "item_home_selected")!
    
        case .messages:
            return UIImage(named: "item_messages_selected")!
        case .scanQR:
            return UIImage(named: "item_messages_selected")!
        case .news:
            return UIImage(named: "item_news_selected")!
        case .personal:
            return UIImage(named: "item_personal_selected")!
        }
    }
    var displayTitle: String {
            return self.rawValue.capitalized(with: nil)
        }
}
