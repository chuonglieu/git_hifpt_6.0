//
//  HomeController.swift
//  Hi FPT
//
//  Created by TaiVo on 30/06/2021.
//

import UIKit

class HomeController: UIViewController {
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewUserInfo: UIView!
    @IBOutlet weak var lblHello: UILabel!
    @IBOutlet weak var btnAvatar: UIButton!
    @IBOutlet weak var imgBigBackground: UIImageView!
    
    var previousContentOffset:CGFloat = 0.0
    
    @IBOutlet weak var clvFunctionUser: UICollectionView!
    @IBOutlet weak var tblHomeMain: UITableView!
    
    let functionUserUtils = [["Hợp đồng","ic_signature"],
                             ["Hóa đơn","ic_bill"],
                             ["Dịch vụ","ic_service"],
                             ["Sản phẩm","ic_product"]
                           ]
    private let sectionInsets = UIEdgeInsets(top: 0.0,
                                             left: 0.0,
                                             bottom: 0.0,
                                             right: 0.0)
    private var itemsPerRow: CGFloat = 4
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        view.layer.backgroundColor = UIColor.systemGray6.cgColor
        self.initUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clvFunctionUser.reloadData()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func initUI() {
        
        clvFunctionUser.contentInsetAdjustmentBehavior = .never
        clvFunctionUser.isPrefetchingEnabled = false
        clvFunctionUser.dataSource = self
        clvFunctionUser.delegate = self
        clvFunctionUser.register(UINib(nibName: "HomeFunctionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeFunctionCell")
       
        self.tblHomeMain.dataSource = self
        self.tblHomeMain.delegate = self
        self.tblHomeMain.register(UINib(nibName: "SaleCell", bundle: nil), forCellReuseIdentifier: "SaleCell")
        self.tblHomeMain.register(UINib(nibName: "PurchasesTableViewCell", bundle: nil), forCellReuseIdentifier: "PurchasesTableViewCell")
        self.tblHomeMain.register(UINib(nibName: "CellMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "CellMenuTBCell")
        self.tblHomeMain.register(UINib(nibName: "BannerTBCell", bundle: nil), forCellReuseIdentifier: "BannerTBCell")
        self.tblHomeMain.register(UINib(nibName: "FunctionHomeCell", bundle: nil), forCellReuseIdentifier: "FunctionHomeCell")
        self.tblHomeMain.register(UINib(nibName: "ProductCell", bundle: nil), forCellReuseIdentifier: "ProductCell")
        self.tblHomeMain.register(UINib(nibName: "HomeUltilsCell", bundle: nil), forCellReuseIdentifier: "HomeUltilsCell")
        self.tblHomeMain.estimatedRowHeight = 200
        self.tblHomeMain.rowHeight = UITableView.automaticDimension
        
        txtSearch.text = "Thanh toán tiền internet"
        
        let attr = NSMutableAttributedString(string: "Xin chào, ", attributes: [.font: UIFont.systemFont(ofSize: 14, weight: .medium),.foregroundColor:UIColor(white: 1, alpha: 0.75)])
        attr.append(NSAttributedString(string: "Trần Quang", attributes: [.font: UIFont.systemFont(ofSize: 14, weight: .bold),.foregroundColor:UIColor.white]))
        lblHello.attributedText = attr
        
        viewSearch.layer.cornerRadius = viewSearch.frame.height/2
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: { [self] in
                //Frame Option 1:
                self.viewUserInfo.frame = CGRect(x: -self.viewUserInfo.frame.width, y: 0, width: self.viewUserInfo.frame.width, height: self.viewUserInfo.frame.height)
                
                self.btnSearch.frame = CGRect(x: 32, y: 0, width: self.btnSearch.frame.width, height: self.btnSearch.frame.height)
                
            },completion: { finish in
                self.btnSearch.isHidden = true
                self.viewUserInfo.isHidden = true
                self.viewSearch.isHidden = false
                self.viewSearch.alpha = 0.1
                UIView.animate(withDuration: 1) {
                    self.viewSearch.transform = self.viewSearch.transform.translatedBy(x: 0.25, y: 0.25)
                    self.viewSearch.alpha = 1
                } completion: { status in
                    //                    self.viewSearch.isHidden = false
                }
            })
            
        }
        
    }
    
}

extension HomeController: UITableViewDataSource {
    
  
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BannerTBCell") as! BannerTBCell
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellMenuTBCell") as! CellMenuTableViewCell
//            cell.lblTitleCell.text = "Ứng dụng yêu thích"
//            cell.initDataFavorite()
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PurchasesTableViewCell") as! PurchasesTableViewCell
         
//            cell.lblTitleCell.text = "Ứng dụng phổ biến"
//            cell.initDataPopular()
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SaleCell") as! SaleCell
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell") as! ProductCell
            cell.lblTitleProduct.text = "Ưu đãi độc quyền Hi-FPT"
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell") as! ProductCell
            cell.lblTitleProduct.text = "Gợi ý cho riêng bạn"
            return cell
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell") as! ProductCell
            cell.lblTitleProduct.text = "Ưu đãi sản phẩm mới"
            return cell
        case 7:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell") as! ProductCell
            cell.lblTitleProduct.text = "Ưu đãi gần bạn"
            return cell
        default:
            return UITableViewCell()
        }
    }
    
}

extension HomeController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("tableView \(indexPath)")
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let widthStartScroll:CGFloat = 10
        let canAnimated:Bool = abs(tblHomeMain.contentOffset.y - previousContentOffset) < 100 ? true : false
        if !canAnimated {
            tblHomeMain.contentOffset.y = previousContentOffset
        }
        else {
            previousContentOffset = tblHomeMain.contentOffset.y
        }
        if tblHomeMain.contentOffset.y > widthStartScroll && canAnimated {
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut) {
                self.imgBigBackground.alpha = 0
                //                self.lblContractTitle.alpha = 0
                //                self.lblFoxpayTitle.alpha = 0
            } completion: { finish in
                //                self.lblContractTitle.isHidden = true
                //                self.lblFoxpayTitle.isHidden = true
            }
        }
        else if tblHomeMain.contentOffset.y <= widthStartScroll && canAnimated {
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut) {
                self.imgBigBackground.alpha = 1
                //                self.lblContractTitle.alpha = 1
                //                self.lblFoxpayTitle.alpha = 1
            } completion: { finish in
                //                self.lblContractTitle.isHidden = false
                //                self.lblFoxpayTitle.isHidden = false
            }
        }
        
    }
    
}

extension HomeController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.functionUserUtils.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeFunctionCell", for: indexPath) as! HomeFunctionCollectionViewCell
        
        cell.lbName.textColor = .white
        
        cell.lbName.text = self.functionUserUtils[indexPath.row][0]
        cell.imgFunction.image = UIImage(named: self.functionUserUtils[indexPath.row][1] )
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 5)
        let availableWidth = clvFunctionUser.bounds.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        return CGSize(width: widthPerItem, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
