//
//  FunctionHomeCLVCell.swift
//  BannerDemoUI6
//
//  Created by Lam Quoc on 30/06/2021.
//

import UIKit

class FunctionHomeCLVCell: UICollectionViewCell {

    @IBOutlet weak var imgFunc: UIImageView!
    @IBOutlet weak var lblFunc: UILabel!
    @IBOutlet weak var lblNumberNoti: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblNumberNoti.layer.borderWidth = 1.5
        lblNumberNoti.layer.borderColor = UIColor.white.cgColor
    }

    func setupAppFavorite(data: HomeFunctionModel) {
        if data.numberOfNoti != "" {
            lblNumberNoti.isHidden = false
            lblNumberNoti.text = data.numberOfNoti
        } else {
            lblNumberNoti.isHidden = true
        }
        lblFunc.text = data.functionName
        imgFunc.image = UIImage(named: data.imgName)
    }
}
