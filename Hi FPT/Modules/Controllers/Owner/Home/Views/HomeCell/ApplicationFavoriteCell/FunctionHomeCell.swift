//
//  FunctionHomeCell.swift
//  BannerDemoUI6
//
//  Created by Lam Quoc on 30/06/2021.
//

import UIKit

class FunctionHomeCell: UITableViewCell {

    @IBOutlet weak var clvFunction: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lblTitleCell: UILabel!
    var functionList:  [HomeFunctionModel] = [HomeFunctionModel]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clvFunction.dataSource = self
        clvFunction.delegate = self
        clvFunction.register(UINib(nibName: "FunctionHomeCLVCell", bundle: nil), forCellWithReuseIdentifier: "FunctionHomeCLVCell")
        
    }

    func initDataFavorite() {
        self.functionList.append(HomeFunctionModel(imgName: "ic_bill", functionName: "Thanh toán hóa đơn", numberOfNoti: "2"))
        self.functionList.append(HomeFunctionModel(imgName: "ic_news", functionName: "F-News", numberOfNoti: ""))
        self.functionList.append(HomeFunctionModel(imgName: "ic_FPT_play", functionName: "FPT Play", numberOfNoti: ""))
        self.functionList.append(HomeFunctionModel(imgName: "ic_Foxpay_wallet", functionName: "Ví Foxpay", numberOfNoti: ""))
        self.functionList.append(HomeFunctionModel(imgName: "ic_Foxpay_wallet", functionName: "Ví Foxpay 2", numberOfNoti: ""))
    }
    
    func initDataPopular() {
        self.functionList.append(HomeFunctionModel(imgName: "ic-Modem", functionName: "Modem", numberOfNoti: ""))
        self.functionList.append(HomeFunctionModel(imgName: "ic-F-beauty", functionName: "F-Beauty", numberOfNoti: ""))
        self.functionList.append(HomeFunctionModel(imgName: "ic-F-Safe", functionName: "FPT-Safe", numberOfNoti: ""))
        self.functionList.append(HomeFunctionModel(imgName: "ic-LongChau", functionName: "Nhà thuốc Long Châu", numberOfNoti: ""))
        self.functionList.append(HomeFunctionModel(imgName: "ic-LongChau", functionName: "Nhà thuốc Long Châu", numberOfNoti: ""))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //let threshold: Int = 100
        let contentOffset: Int = Int(scrollView.contentOffset.x)
        let maximumOffset: Int = Int(scrollView.contentSize.width - scrollView.frame.size.width)
        if (contentOffset >= maximumOffset ){
            //Your code
            print(contentOffset)
            self.pageControl.currentPage = self.pageControl.currentPage + 1
        } else if contentOffset <= 0 {
            print(contentOffset)
            self.pageControl.currentPage = self.pageControl.currentPage - 1
        }
    }
    
}
extension FunctionHomeCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.functionList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FunctionHomeCLVCell", for: indexPath) as! FunctionHomeCLVCell
        cell.setupAppFavorite(data: self.functionList[indexPath.row])
//        print("AAAAAA")
        print(Int(floor(clvFunction.contentSize.width/clvFunction.frame.size.width)))
        pageControl.numberOfPages = Int(floor(clvFunction.contentSize.width/clvFunction.frame.size.width)) + 1
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let noOfCellsInRow = 4.3
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))

        return CGSize(width: size, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }

    func collectionView(_ lectionView: UICollectionView, layout
                            collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt minminimumLineSpacingForSectionAtction: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("collection \(indexPath.row)")
    }
}

struct HomeFunctionModel {
    var imgName: String = ""
    var functionName: String = ""
    var numberOfNoti: String = ""
}
