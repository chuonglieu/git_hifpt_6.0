//
//  CellMenuTableViewCell.swift
//  Hi FPT
//
//  Created by Developer on 05/07/2021.
//

import UIKit

class CellMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var clvMenu: UICollectionView!
    
    @IBOutlet weak var heightContrainstCollect: NSLayoutConstraint!
 
    let menuUtils = [["Vé máy bay","ic_ticket"],
                     ["Nhà thuốc Long Châu","ic_fpt_longchau"],
                             ["FPT Play","ic_fpt_play"],
                             ["FPT Truyền hình","ic_fpt_tv"],
                             ["Tiền điện","ic_electric"],
                             ["HD Insurance","ic_hd_i"],
                             ["F-Beauty","ic_f_beauty"],
                             
                            
                             
                             ["Thêm","ic_other"]]
    private let sectionInsets = UIEdgeInsets(top: 0.0,
                                             left: 0.0,
                                             bottom: 0.0,
                                             right: 0.0)
    private var itemsPerRow: CGFloat = 4
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.heightContrainstCollect.constant = CGFloat(menuUtils.count / 4 * 120)
        // Initialization code
        clvMenu.contentInsetAdjustmentBehavior = .never
        clvMenu.isPrefetchingEnabled = false
        clvMenu.dataSource = self
        clvMenu.delegate = self
        clvMenu.register(UINib(nibName: "MenuCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MenuCLVCell")
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        clvMenu.reloadData()
    }
    

}

extension CellMenuTableViewCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.menuUtils.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCLVCell", for: indexPath) as! MenuCollectionViewCell
        
       
        cell.lbMenu.text = self.menuUtils[indexPath.row][0]
        cell.imgMenu.image = UIImage(named: self.menuUtils[indexPath.row][1] )
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 5)
        let availableWidth = clvMenu.bounds.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        return CGSize(width: widthPerItem, height: 120)
      
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
