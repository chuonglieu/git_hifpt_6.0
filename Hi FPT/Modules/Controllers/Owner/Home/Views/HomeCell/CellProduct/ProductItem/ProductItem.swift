//
//  ProductItem.swift
//  Hi FPT
//
//  Created by Lam Quoc on 01/07/2021.
//

import UIKit

class ProductItem: UICollectionViewCell {

    
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblNameProduct: UILabel!
    @IBOutlet weak var imgType: UIImageView!
    @IBOutlet weak var lblNameType: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        self.contentView.layer.cornerRadius = 15
        self.contentView.layer.masksToBounds = true
    }

}
