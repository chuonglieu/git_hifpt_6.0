//
//  ProductCell.swift
//  Hi FPT
//
//  Created by Lam Quoc on 01/07/2021.
//

import UIKit

class ProductCell: UITableViewCell {

    @IBOutlet weak var clvProduction: UICollectionView!
    @IBOutlet weak var lblTitleProduct: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clvProduction.register(UINib(nibName: "ProductItem", bundle: nil), forCellWithReuseIdentifier: "ProductItem")
        clvProduction.delegate = self
        clvProduction.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension ProductCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //ic-Market
        //ic-LongChau
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductItem", for: indexPath) as! ProductItem
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if #available(iOS 11.0, *), UIScreen.main.bounds.width <= 320 {
            let width = UIScreen.main.bounds.width
            let cellWidth = (width) / 2
            return CGSize(width: cellWidth, height: cellWidth * 262/170)
        }
        
        let width = self.contentView.frame.width - 20
        let cellWidth = (width - 30) / 2
        return CGSize(width: cellWidth, height: cellWidth * 262/170)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("ProductCell - \(indexPath.row)")
    }
    
}
