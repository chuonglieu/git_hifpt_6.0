//
//  HomeUltilsCell.swift
//  Hi FPT
//
//  Created by Lam Quoc on 01/07/2021.
//

import UIKit

class HomeUltilsCell: UITableViewCell {

    @IBOutlet weak var clvButtonUltils: UICollectionView!
    @IBOutlet weak var clvFunctionUltils: UICollectionView!
    @IBOutlet weak var lblTitleUltils: UILabel!
    
    let fakeTitleUltils = ["Ví điện tử", "Giải trí", "Mua sắm", "Học tập", "Tin tức"]
    var functionList:  [HomeFunctionModel] = [HomeFunctionModel]()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clvButtonUltils.dataSource = self
        clvButtonUltils.delegate = self
        clvButtonUltils.register(UINib(nibName: "UltilsButtonItem", bundle: nil), forCellWithReuseIdentifier: "UltilsButtonItem")
        
        clvFunctionUltils.dataSource = self
        clvFunctionUltils.delegate = self
        clvFunctionUltils.register(UINib(nibName: "FunctionHomeCLVCell", bundle: nil), forCellWithReuseIdentifier: "FunctionHomeCLVCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initDataFavorite() {
        self.functionList.append(HomeFunctionModel(imgName: "ic_bill", functionName: "Thanh toán hóa đơn", numberOfNoti: "2"))
        self.functionList.append(HomeFunctionModel(imgName: "ic_news", functionName: "Chuyển tiền", numberOfNoti: ""))
        self.functionList.append(HomeFunctionModel(imgName: "ic_FPT_play", functionName: "Mua thẻ cào", numberOfNoti: ""))
        self.functionList.append(HomeFunctionModel(imgName: "ic_Foxpay_wallet", functionName: "Nạp điện thoại", numberOfNoti: ""))
        self.functionList.append(HomeFunctionModel(imgName: "ic_Foxpay_wallet", functionName: "Ví Foxpay 2", numberOfNoti: ""))
    }
}
extension HomeUltilsCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == clvButtonUltils {
            return self.fakeTitleUltils.count
        } else {
            //clvFunctionUltils
            return self.functionList.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == clvButtonUltils {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UltilsButtonItem", for: indexPath) as! UltilsButtonItem
            if indexPath.row != 0 {
                cell.lblNameUltils.backgroundColor = .white
                cell.lblNameUltils.textColor = .black
            }
            cell.lblNameUltils.text = self.fakeTitleUltils[indexPath.row]
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FunctionHomeCLVCell", for: indexPath) as! FunctionHomeCLVCell
            cell.setupAppFavorite(data: self.functionList[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == clvButtonUltils {
            let width = self.contentView.frame.width - 20
            let cellWidth = (width - 30) / 3.5
            return CGSize(width: cellWidth, height: cellWidth * 110/50)
        } else {
            let width = self.contentView.frame.width - 20
            let cellWidth = (width - 25) / 4
            return CGSize(width: cellWidth, height: cellWidth * 126/80)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }

    func collectionView(_ lectionView: UICollectionView, layout
                            collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt minminimumLineSpacingForSectionAtction: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("collection \(indexPath.row)")
    }
}
