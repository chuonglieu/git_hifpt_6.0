//
//  PurchasesCollectionViewCell.swift
//  Hi FPT
//
//  Created by Developer on 05/07/2021.
//

import UIKit

class PurchasesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var viewDashLine: UIView!
    @IBOutlet weak var btPay: UIButton!
    
    @IBOutlet weak var lbActuallyPrice: UILabel!
    @IBOutlet weak var lbSalePrice: UILabel!
    @IBOutlet weak var lbContent: UILabel!
    @IBOutlet weak var lbTittle: UILabel!
    @IBOutlet weak var viewRightDot: UIView!
    @IBOutlet weak var viewLeftDot: UIView!
    @IBOutlet weak var viewCollection: UIView!
    
    @IBOutlet weak var btRemove: UIButton!
    @IBOutlet weak var viewTopButton: UIView!
    
    typealias removeItem = (Int)->Void
    var removeItemPurchase: removeItem = { index in
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
     
        viewDashLine.makeDashedBorderLine()
        btRemove.roundCorner(borderColor: .clear, borderWidth: 0, cornerRadius: 15)
        viewLeftDot.roundCorner(borderColor: .clear, borderWidth: 0, cornerRadius: viewLeftDot.frame.width/2)
        viewRightDot.roundCorner(borderColor: .clear, borderWidth: 0, cornerRadius: viewRightDot.frame.width/2)
        viewCollection.roundCorner(borderColor: .clear, borderWidth: 0, cornerRadius: 15)
        btPay.roundCorner(borderColor: .clear, borderWidth: 0, cornerRadius: 10)
    }
    @IBAction func btRemoveClick(_ sender: Any) {
        guard let control = sender as? UIControl else {
            return
        }
        removeItemPurchase(control.tag)
    }
}
