//
//  PurchasesTableViewCell.swift
//  Hi FPT
//
//  Created by Developer on 05/07/2021.
//

import UIKit

class PurchasesTableViewCell: UITableViewCell {

    @IBOutlet weak var heightConstraintCollectionView: NSLayoutConstraint!
    @IBOutlet weak var clvPurchases: UICollectionView!
    var purchaseUtils = [["F-News","person"], ["F-News","person"]]
    private let sectionInsets = UIEdgeInsets(top: 0.0,
                                             left: 10.0,
                                             bottom: 0.0,
                                             right: 10.0)
    private var itemsPerRow: CGFloat = 4
    typealias reload = (Bool)->Void
    var reloadTableView:reload = { (result) in
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
      
        // Initialization code
        clvPurchases.contentInsetAdjustmentBehavior = .never
        clvPurchases.isPrefetchingEnabled = false
        clvPurchases.dataSource = self
        clvPurchases.delegate = self
        clvPurchases.register(UINib(nibName: "PurchasesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PurchasesCollectionViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension PurchasesTableViewCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.purchaseUtils.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PurchasesCollectionViewCell", for: indexPath) as! PurchasesCollectionViewCell
        cell.btRemove.tag = indexPath.row
        cell.removeItemPurchase = { [weak self] index in
            print(index)
            self?.purchaseUtils.remove(at: index)
            self?.clvPurchases.reloadData()
          
        }
       
//        cell.lbMenu.text = self.purchaseUtils[indexPath.row][0]
//        cell.imgMenu.image = UIImage(systemName: self.menuUtils[indexPath.row][1] )
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let availableWidth = clvPurchases.bounds.width - 20
        let widthPerItem = availableWidth - 40
        return CGSize(width: widthPerItem, height: floor(160))
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
