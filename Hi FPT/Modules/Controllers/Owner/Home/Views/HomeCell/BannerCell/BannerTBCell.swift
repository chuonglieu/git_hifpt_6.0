//
//  BannerTBCell.swift
//  BannerDemoUI6
//
//  Created by Lam Quoc on 30/06/2021.
//

import UIKit

class BannerTBCell: UITableViewCell {
    
    
    @IBOutlet weak var clvBanner: UICollectionView!
    @IBOutlet weak var pageControl: DefaultPageControl!
    lazy var nameImg: [String] = ["B1", "B2", "B3"]
    
    var timerSlider: Timer = Timer()
    var index = 0
    private let sectionInsets = UIEdgeInsets(top: 0.0,
                                             left: 0.0,
                                             bottom: 0.0,
                                             right: 0.0)
    private var itemsPerRow: CGFloat = 1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        clvBanner.dataSource = self
        clvBanner.delegate = self
        clvBanner.register(UINib(nibName: "BannerHomeCLVCell", bundle: nil), forCellWithReuseIdentifier: "BannerHomeCLVCell")
        timerSlider = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(changeSlide), userInfo: nil, repeats: true)
        //pageControl.pageIndicatorTintColor = UIColor.gray.withAlphaComponent(0.7)//UIColor(cgColor: CGColor(red: 85, green: 117, blue: 251, alpha: 0.35))
        
        //pageControl.currentPageIndicatorTintColor = UIColor.blue.withAlphaComponent(0.6)//UIColor(cgColor: CGColor(red: 85, green: 117, blue: 251, alpha: 1.0))
        pageControl.numberOfPages = nameImg.count
        pageControl.currentPage = 0
        pageControl.isEnabled = false
    }
    
    
    @IBAction func pageControlSelectionAction(_ sender: UIPageControl) {
        let page: Int? = sender.currentPage
        var frame: CGRect = self.clvBanner.frame
        frame.origin.x = frame.size.width * CGFloat(page ?? 0)
        frame.origin.y = 0
        for i in 0...self.nameImg.count-1 {
            let image = page == i ? UIImage.init(named: "dot_selected") : UIImage.init(named: "dot_nomal")
            pageControl.setIndicatorImage(image, forPage: i)
        }
        self.clvBanner.scrollRectToVisible(frame, animated: true)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @objc func changeSlide() {
        index = index + 1
        if index < self.nameImg.count {
            pageControl.currentPage = index
            clvBanner.scrollToItem(at: IndexPath(item: index, section: 0), at: .right, animated: true)
        }
        else {
            index = 0
            pageControl.currentPage = index
            clvBanner.scrollToItem(at: IndexPath(item: index, section: 0), at: .right, animated: true)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
        print("page = \(page)")
        pageControl.currentPage = page
    }
    
}

extension BannerTBCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.nameImg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerHomeCLVCell", for: indexPath) as! BannerHomeCLVCell
        cell.imgAdv.image = UIImage(named: self.nameImg[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 5)
        let availableWidth = clvBanner.bounds.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        return CGSize(width: widthPerItem, height: 157)
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("collection \(indexPath.row)")
    }
}



class DefaultPageControl: UIPageControl {
    override func layoutSubviews() {
        super.layoutSubviews()
        //self.currentPage.view
        
        
    }
    override var numberOfPages: Int {
        didSet {
            self._updateIndicators()
        }
    }
    
    override var currentPage: Int {
        didSet {
            self._updateIndicators()
        }
    }
    
    private func _updateIndicators() {
        var indicators: [UIView] = []
        
        if #available(iOS 14.0, *) {
            indicators = self.subviews.first?.subviews.first?.subviews ?? []
        } else {
            indicators = self.subviews
        }
        
        for (index, indicator) in indicators.enumerated() {
            let image = self.currentPage == index ? UIImage.init(named: "dot_selected") : UIImage.init(named: "dot_nomal")
            if let dot = indicator as? UIImageView {
                dot.image = image
                indicator.tintColor = .white
                
            } else {
                let imageView = UIImageView.init(image: image)
                indicator.addSubview(imageView)
                indicator.tintColor = .white
                indicator.alpha = 0.5
                // here you can add some constraints to fix the imageview to his superview
            }
        }
    }
    
}
